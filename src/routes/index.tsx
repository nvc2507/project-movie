import { AuthLayout } from 'components/layouts'
import { MainLayout } from 'components/templates'
import { PATH } from 'constant'
import { Login, Register, Home, Account, DetailMovie, Purchase } from 'pages'
import { RouteObject } from 'react-router-dom'


export const router:RouteObject[]  = [
    {
        path: '/',
        element: <MainLayout></MainLayout>,
        children: [
            {
                index: true,
                element: <Home></Home>
            },
           
            {
                path: PATH.detailmovie,
                element: <DetailMovie></DetailMovie>
            },
            {
                path: PATH.account,
                element: <Account />,
              },
            {
                path: PATH.purchase,
                element: <Purchase />,
              },
        ]
    },
    {
        element: <AuthLayout></AuthLayout>,
        children: [
            {
                path: PATH.login,
                element: <Login></Login>
            },
            {
                path: PATH.register,
                element: <Register></Register>
            }
        ]
    }
]