import { useSelector } from "react-redux"
import { RootState } from "store"

export const useMovie = () => { 
    const { moiveList, bannerList, isFetchingMovieList, currentPage } = useSelector((state: RootState) => state.quanLyPhim);
    return { moiveList, bannerList, isFetchingMovieList, currentPage };
}