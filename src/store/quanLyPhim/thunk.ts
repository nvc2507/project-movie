import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyPhimServices } from "services/quanLyPhim";


export const getMovieListThunk = createAsyncThunk('quanLyPhim/LayDanhSachPhimPhanTrang', async (page: number,{rejectWithValue})=> {
    try {
        const query = `?maNhom=GP01&soTrang=${page}&soPhanTuTrenTrang=10`;
        const data = await quanLyPhimServices.getMovieList(`${query}`)

        // Sleep
        await new Promise((resolve)=> setTimeout(resolve, 500))
        return data.data.content.items;
            
            
    } catch (error) {
        return rejectWithValue(error)
    }
})

export const getBannerListThunk = createAsyncThunk('quanLyPhim/getBannerListThunk', async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyPhimServices.getBannerList();
        return data.data.content;
    } catch (error) {
        return rejectWithValue(error)
    }
})

