import { createSlice } from "@reduxjs/toolkit";
import { Banner, Movie } from "types";
import { getBannerListThunk, getMovieListThunk } from "./thunk";

type QuanLyPhimState = {
    moiveList?: Movie[],
    isFetchingMovieList?: boolean,
    bannerList?: Banner[],
    currentPage: number;
}

const initialState: QuanLyPhimState = {
    moiveList: [],
    isFetchingMovieList: false,
    bannerList: [],
    currentPage: 1,
}

const quanLyPhimSlice = createSlice({
    name: 'QuanLyPhim',
    initialState,
    reducers: {setCurrentPage: (state, action) => {
        state.currentPage = action.payload;
      },},
    extraReducers: (builder) => { 
        builder
            .addCase(getMovieListThunk.pending, (state) => {
                state.isFetchingMovieList = true;
            })
            .addCase(getMovieListThunk.fulfilled, (state, {payload}) => { 
                state.moiveList = payload;
                state.isFetchingMovieList = false;
            })
            .addCase(getMovieListThunk.rejected, (state) => {
                state.isFetchingMovieList = false;
            })
            .addCase(getBannerListThunk.fulfilled, (state, {payload}) => {
                state.bannerList = payload;
            })
    }
})

export const { reducer: quanLyPhimReducer, actions: quanLyPhimActions } = quanLyPhimSlice;