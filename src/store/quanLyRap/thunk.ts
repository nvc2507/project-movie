import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyRapServices } from "services/quanLyRap";

export const getMovieTheaterListThunk = createAsyncThunk('quanLyRap/getMovieTheaterListThunk', async (_,{rejectWithValue}) => { 
    try {
        const data = await quanLyRapServices.getDetailMovieTheater();
        return data.data.content;
    } catch (error) {
        return rejectWithValue(error);
    }
})

export const layThongTinLichChieuHeThongRapThunk = createAsyncThunk('quanLyRap/layThongTinLichChieuHeThongRapThunk', async (_, {rejectWithValue}) => {
    try {
        const data = await quanLyRapServices.layThongTinLichChieuHeThongRap();
        return data.data.content;
    } catch (error) {
        return rejectWithValue(error)
    }
})

export const layThongTinLichChieuPhimThunk = createAsyncThunk('quanLyRap/layThongTinLichChieuPhimThunk', async (payload: string, {rejectWithValue}) => {
    try {
        const data = await quanLyRapServices.layThongTinLichChieuPhim(payload);
        return data.data.content;
    } catch (error) {
        return rejectWithValue(error);
    }
})