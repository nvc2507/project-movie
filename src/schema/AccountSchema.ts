import { z } from 'zod';

export const AccountSchema = z.object({
    taiKhoan:   z.string()
                .nonempty('Vui lòng nhập tài khoản')
                .min(6,'Tài khoản cần ít nhất 6 kí tự')
                .max(20,'Tài khoản cần tối đa 20 kí tự'),
    matKhau:    z.string()
                .nonempty('Vui lòng nhập mật khẩu'),
    email:      z.string()
                .nonempty('Vui lòng nhập email')
                .email('Vui lòng nhập đúng dạng email'),
    soDt:       z.string()
                .nonempty('Vui lòng nhập số điện thoại'),
    maNhom:     z.string()
                .nonempty('Vui lòng nhập mã nhóm'),
    hoTen:      z.string()
                .nonempty('Vui lòng nhập họ và tên'),
    maLoaiNguoiDung: z.string()
                     .nonempty('Vui lòng nhập loại người dùng')
});

export type AccountSchemaType = z.infer<typeof AccountSchema>;