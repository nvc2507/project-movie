import z from 'zod'

export const LoginSchema = z.object({
    taiKhoan: z.string()
            .nonempty('Vui lòng nhập tài khoản')
            .min(6,'Tài khoản cần ít nhất 6 kí tự')
            .max(20,'Tài khoản cần tối đa 20 kí tự'),
    matKhau:  z.string()
             .nonempty('Vui lòng nhập mật khẩu'),
})

export type LoginSchemaType = z.infer<typeof LoginSchema>;