import axios, { CreateAxiosDefaults, AxiosRequestHeaders } from 'axios'

const TOKEN_CYBERSOFT = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0NiIsIkhldEhhblN0cmluZyI6IjMxLzAxLzIwMjQiLCJIZXRIYW5UaW1lIjoiMTcwNjY1OTIwMDAwMCIsIm5iZiI6MTY3ODI5NDgwMCwiZXhwIjoxNzA2ODA2ODAwfQ.RmFBx9ElL7VuYNzZnzMoGUHyC3iXKRpw7Yvq2LsXk0Q';
const AUTHORIZATION_CYBERSOFT = "Bearer" + " " + "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiVGhhbmgxMjM0NTYiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9lbWFpbGFkZHJlc3MiOiJwb2tvenlieXhlQG1haWxpbmF0b3IuY29tIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjpbIktoYWNoSGFuZyIsInBva296eWJ5eGVAbWFpbGluYXRvci5jb20iLCJHUDAwIl0sIm5iZiI6MTY5NDIyNjQ3NiwiZXhwIjoxNjk0MjMwMDc2fQ.TLzh722rs1AkchkGS9RErL1DDdNraHtFLJdhfFnkwdc";

export const apiInstance = (config?:CreateAxiosDefaults) => { 
    const api = axios.create(config);
    api.interceptors.request.use((config) => { 
        return {
            ... config,
            headers: {
                TokenCybersoft: TOKEN_CYBERSOFT,
                Authorization: AUTHORIZATION_CYBERSOFT,
            } as unknown as AxiosRequestHeaders
        }
    })
    return api;
}