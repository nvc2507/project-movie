export type User =  {
  taiKhoan: string
  hoTen: string
  email: string
  soDT: string
  maNhom: string
  maLoaiNguoiDung: string
  accessToken: string
}

export type UserInfo = {
  taiKhoan: string
  matKhau: string
  email: string
  soDt: string
  maNhom: string
  maLoaiNguoiDung: string
  hoTen: string
}