export type MovieTheater = {
    maHeThongRap: string    
    tenHeThongRap: string
    biDanh: string
    logo: string
};

export type LichChieu = {
    maLichChieu: number
    maRap: number
    tenRap: string
    ngayChieuGioChieu: string
    giaVe: number
}
export type Phim = {
    lstLichChieuTheoPhim: LichChieu[]
    maPhim: number
    tenPhim: string
    hinhAnh: string
    hot: true
    dangChieu: true
    sapChieu: true
}
export type CumRap = {
    danhSachPhim: Phim[]
    maCumRap: string
    tenCumRap: string
    diaChi: string
    hinhAnh: string
} 
export type HeThong = {
    lstCumRap: CumRap[]
    maHeThongRap: string
    tenHeThongRap: string
    logo: string
    mahom: string
}

export type LichChieuPhim = {
    maLichChieu: number
    maRap: number
    tenRap: string
    ngayChieuGioChieu: string
    giaVe: number
    thoiLuong: number
}
export type CumRapChieu = {
    lichChieuPhim: LichChieuPhim[],
    maCumRap: string
    tenCumRap: string
    diaChi: string
    hinhAnh: string
}
export type HeThongRapChieu = {
    cumRapChieu: CumRapChieu[]
    maHeThongRap: string
    tenHeThongRap: string
    logo: string
}
export type ThongTinChiTiet = {
    heThongRapChieu: HeThongRapChieu[]
    maPhim: number;
    tenPhim: string;
    biDanh: string;
    trailer: string;
    hinhAnh: string;
    moTa: string;
    maNhom: string;
    hot: boolean;
    dangChieu: boolean;
    sapChieu: boolean;
    ngayKhoiChieu: string;
    danhGia: number;
} 
