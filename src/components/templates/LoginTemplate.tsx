import { useNavigate } from 'react-router-dom'
import { PATH } from 'constant/config';
import { useForm, SubmitHandler } from 'react-hook-form'
import { LoginSchema, LoginSchemaType } from 'schema/LoginSchema';
import { zodResolver } from '@hookform/resolvers/zod';
// import { quanLyNguoiDungServices } from 'services';
import { toast } from 'react-toastify'
import { useAppDispatch } from 'store';
import { loginThunk } from 'store/quanLyNguoiDung/thunk';
import Input from 'components/ui/Input';

export const LoginTemplate = () => {
  const navigate = useNavigate();
  const { handleSubmit, formState: {errors}, register } = useForm<LoginSchemaType>({
    mode: 'onChange',
    resolver: zodResolver(LoginSchema),
  })

  const dispatch = useAppDispatch();


  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => { 

    
    dispatch(loginThunk(value)).unwrap().then(() => { 
      toast.success('Đăng nhập thành công');
      
      navigate('/');
     });
  }
  return (
    <form className="px-6 pb-20 pt-6" onSubmit={handleSubmit(onSubmit)}>
        <h2 className="text-white text-[32px] font-600">Đăng nhập</h2>
        <div className="mt-8">
            <Input name='taiKhoan' type='text' placeholder='Tài khoản' register={register} error={errors?.taiKhoan?.message}></Input>
        </div>
        <div className="mt-4">
            <Input name='matKhau' type='password' placeholder='Mật khẩu' register={register} error={errors?.matKhau?.message}></Input>
        </div>
        <div className="mt-10">
            <button className="w-full h-14 outline-none font-600 text-white bg-red-700 hover:bg-red-800 rounded-lg text-sm px-5 py-2.5 mr-2 mb-2">Đăng nhập</button>
            <p className="text-gray-400 mt-2">Bạn chưa có tài khoản? <span className="font-500 text-white hover:cursor-pointer" onClick={() => { navigate(PATH.register) }}>Đăng ký ngay</span></p>
        </div>
    </form>
  )
}

export default LoginTemplate