import Footer from "components/ui/Footer"
import Header from "components/ui/Header"
import { Outlet } from "react-router-dom"
import { styled } from "styled-components"

export const MainLayout = () => {
  return (
    <div>
        <Header></Header>
        <Container>
            <Outlet></Outlet>
        </Container>
        <Footer/>
    </div>
  )
}

export default MainLayout

const Container = styled.main`
    max-width: var(--max-width);
    margin: 0 auto;
`