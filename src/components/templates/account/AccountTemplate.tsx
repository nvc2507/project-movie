import { Tabs } from 'components/ui'
import AccountInfoTab from './AccountInfoTab'
import { AccountBooking } from '.'

export const AccountTemplate = () => {
    const accessTokenJson = localStorage.getItem('accessToken');
    if(accessTokenJson){
        var accessToken = JSON.parse(accessTokenJson);
    }
    if(!accessToken) return;
    return (
        <div className='pt-[90px] pb-[50px]'>
            <h2 className=' uppercase text-center text-zinc-500 font-bold lg:text-4xl'>Thông tin Tài khoản</h2>
            <Tabs
                tabPosition="top"
                className="h-full "
                tabBarGutter={-5}
                items={[
                    {
                        label: (
                            <div className="w-[150px] text-left font-bold hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3 text-black">
                                Thông tin tài khoản
                            </div>
                        ),
                        key: 'accountInfo',
                        children: <AccountInfoTab />,
                    },
                    {
                        label: (
                            <div className="w-[150px] text-left font-bold  hover:bg-gray-400 hover:text-white rounded-lg transition-all p-3 text-black">
                                Thông tin vé đã đặt
                            </div>
                        ),
                        key: 'ticketInfo',
                        children: <AccountBooking />,
                    },
                ]}
            />
        </div>
    )
}

export default AccountTemplate


