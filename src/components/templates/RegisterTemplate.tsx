import { useNavigate } from 'react-router-dom'
import { PATH } from 'constant/config';
import { useForm, SubmitHandler } from 'react-hook-form'
import { RegisterSchema, RegisterSchemaType } from 'schema/RegisterSchema';
import { zodResolver } from '@hookform/resolvers/zod';
import { quanLyNguoiDungServices } from 'services';
import { toast } from 'react-toastify'
import Input from 'components/ui/Input';

export const RegisterTemplate = () => {
  const navigate = useNavigate();

  const { handleSubmit, formState: {errors}, register } = useForm({
    mode: 'onChange',
    resolver: zodResolver(RegisterSchema)
  });
  
  const onSubmit: SubmitHandler<RegisterSchemaType> = async (value) => {
    try {
      console.log(value);
      await quanLyNguoiDungServices.register(value);
      toast.success('Đăng ký thành công');
      navigate(PATH.login);
    } catch (error) {
      toast.error(error?.response?.data?.content);
    }
  };

  return (
    <form className="px-6 pb-2" onSubmit={handleSubmit(onSubmit)}>
        <h2 className="text-white text-[32px] font-600">Đăng nhập</h2>
        <div className="mt-4">
            <Input name='taiKhoan' type='text' placeholder='Tài khoản' register={register} error={errors?.taiKhoan?.message as string} />
        </div>
        <div className="mt-3">
            <Input name='matKhau' type='password' placeholder='Mật khẩu' register={register} error={errors?.matKhau?.message as string}></Input>
        </div>
        <div className="mt-3">
            <Input name='email' type='text' placeholder='Email' register={register} error={errors?.email?.message as string}></Input>
            </div>
        <div className="mt-3">
            <Input name='soDt' type='text' placeholder='Số điện thoại' register={register} error={errors?.soDt?.message as string}></Input>
        </div>
        <div className="mt-3">
            <Input name='maNhom' type='text' placeholder='Mã nhóm' register={register} error={errors?.maNhom?.message as string}></Input>
        </div>
        <div className="mt-3">
            <Input name='hoTen' type='text' placeholder='Họ và tên' register={register} error={errors?.hoTen?.message as string}></Input>
        </div>
        
        <div className="mt-4">
            <button className="w-full h-14 outline-none font-600 text-white bg-red-700 hover:bg-red-800 rounded-lg text-sm px-5 py-2.5 mr-2 mb-2">Đăng ký</button>
            <p className="text-gray-400 mt-2">Bạn đã có tài khoản? <span className="font-500 text-white hover:cursor-pointer" onClick={() => { navigate(PATH.login) }}>Đăng nhập</span></p>
        </div>
    </form>
  )
}

export default RegisterTemplate