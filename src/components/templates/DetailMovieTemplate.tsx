import { PATH } from "constant";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { generatePath, useNavigate, useParams } from "react-router-dom";
import { RootState, useAppDispatch } from "store";
import {
  getMovieTheaterListThunk,
  layThongTinLichChieuPhimThunk,
} from "store/quanLyRap/thunk";

export const DetailMovieTemplate = () => {
  const { thongTinLichChieu, movieTheaterList } = useSelector(
    (state: RootState) => state.quanLyRap
  );
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const [isModalOpen, setIsModalOpen] = useState(false);

  const { movieId } = useParams();

  // Hàm xử lý mở modal
  const openModal = () => {
    setIsModalOpen(true);
  };

  // Hàm xử lý đóng modal
  const closeModal = () => {
    setIsModalOpen(false);
  };

  useEffect(() => {
    dispatch(layThongTinLichChieuPhimThunk(movieId));
    dispatch(getMovieTheaterListThunk());
  }, [dispatch]);

  return (
    <div>
      <div className="lg:grid grid-cols-8 mt-[80px] bg-blue-950">
        <div className="mt-6 ml-3 pb-3  col-span-2">
          <img
            className="pt-6 px-6"
            src={thongTinLichChieu?.hinhAnh}
            alt="..."
          />
        </div>
        <div className="col-span-6 ml-3 text-white mt-6">
          <div className="flex justify-between">
            <h2 className="font-600 text-30">{thongTinLichChieu?.tenPhim}</h2>
            {thongTinLichChieu?.hot && (
              <p
                className="bg-red-700 font-700 text-20 mr-6 text-center"
                style={{
                  height: 30,
                  width: 60,
                }}
              >
                HOT
              </p>
            )}
          </div>
          <p className="mt-4 text-gray-400">{thongTinLichChieu?.moTa}</p>
          <p className="text-red-600 mt-8">
            {" "}
            <span className="font-500 text-white mr-24">Thể loại:</span>{" "}
            C18-PHIM DÀNH CHO KHÁN GIẢ TỪ 18 TUỔI TRỞ LÊN{" "}
          </p>
          <p className="mt-4">
            <span className="font-500 text-white mr-10">Ngày khởi chiếu:</span>
            {thongTinLichChieu?.ngayKhoiChieu}
          </p>
          {(thongTinLichChieu?.dangChieu || thongTinLichChieu?.sapChieu) && (
            <p className="mt-4">
              <span className="font-500 text-white mr-82">Tình trạng:</span>
              {thongTinLichChieu?.dangChieu ? "Đang chiếu" : "Sắp chiếu"}
            </p>
          )}
          <div className="flex">
            <div className="flex mt-10 pb-10">
              <button
                className=" bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-3 rounded"
                onClick={openModal}
              >
                XEM TRAILER
              </button>
              <button className="ml-3 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-3 rounded">
                MUA VÉ XEM PHIM
              </button>
            </div>
          </div>
          {/* Modal */}
          {isModalOpen && (
            <div className="modal">
              <div className="modal-content">
                {/* Nút đóng modal */}
                <button
                  className="close-button text-red-500 font-700 text-20"
                  onClick={closeModal}
                >
                  x
                </button>

                {/* Nội dung modal */}
                {/* Đặt nội dung video trailer hoặc iframe chứa video trailer */}
                <iframe
                  width="560"
                  height="315"
                  src={thongTinLichChieu.trailer}
                  title="Trailer"
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </div>
            </div>
          )}
        </div>
      </div>
      <div className=" mt-3">
        <div className="grid lg:grid-cols-6 md:grid-cols-3 grid-cols-2 px-10">
          {movieTheaterList.map((rapChieu) => {
            return (
              <div
                className="flex justify-center text-center mb-3"
                key={rapChieu.maHeThongRap}
              >
                <img
                  style={{
                    height: 80,
                    width: 80,
                  }}
                  src={rapChieu.logo}
                  alt="..."
                />
              </div>
            );
          })}
        </div>
        <div className="col-span-3">
          {thongTinLichChieu?.heThongRapChieu?.map((rapChieu) => {
            return rapChieu?.cumRapChieu?.map((cumRap) => {
              return (
                <div key={cumRap.maCumRap}>
                  <div className="flex">
                    <img
                      className="mr-3"
                      style={{
                        height: 80,
                        width: 100,
                      }}
                      src={cumRap?.hinhAnh}
                      alt="..."
                    />
                    <div>
                      <h2 className="text-20 mt-1">{cumRap?.tenCumRap}</h2>
                      <p className="mt-1 text-gray-500">
                        {cumRap?.diaChi}{" "}
                        <span className="text-red-500">(Bản đồ)</span>
                      </p>
                    </div>
                  </div>
                  <div className="grid grid-cols-8">
                    <div className="col-span-1 text-center py-3 text-30 text-gray-600 font-700">
                      2D
                    </div>
                    <div className="col-span-7 flex w-40">
                      {cumRap?.lichChieuPhim?.map((lichChieu, index: number) => {
                        if(index < 3) {
                          const path = generatePath(PATH.purchase, {
                            maLichChieu: lichChieu.maLichChieu,
                          });
                          return (
                            <div className="mr-2" key={lichChieu?.maLichChieu}>
                              <div
                                className=" text-20 text-green-600 font-500 cursor-pointer"
                                onClick={() => {
                                  navigate(path);
                                }}
                              >
                                <p className="font-bold ml-3">
                                  {" "}
                                  {lichChieu.ngayChieuGioChieu.substring(6, 10)}
                                </p>
                                <p className="font-bold ml-3">
                                  {" "}
                                  {lichChieu.ngayChieuGioChieu.substring(11, 16)}
                                </p>
                              </div>
                            </div>
                          )
                        };
                      })}
                    </div>
                  </div>
                </div>
              );
            });
          })}
        </div>
      </div>
    </div>
  );
};

export default DetailMovieTemplate;
