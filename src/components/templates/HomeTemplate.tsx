import { PATH } from "constant";import { Card, Skeleton, Carousel, Detail, Pagination } from "components/ui";

import { useMovie } from "hooks";
import { useEffect } from "react";
import { generatePath, useNavigate } from "react-router-dom";
import { useAppDispatch } from "store"
import { getBannerListThunk, getMovieListThunk } from "store/quanLyPhim/thunk";
import { quanLyPhimActions } from "store/quanLyPhim/slice";
import { getMovieTheaterListThunk, layThongTinLichChieuHeThongRapThunk } from "store/quanLyRap/thunk";



const HomeTemplate: React.FC = () => {
    const dispatch = useAppDispatch();
    const { moiveList, isFetchingMovieList, bannerList , currentPage} = useMovie();
    const navigate = useNavigate()
    
    useEffect(() => { 
        dispatch(getMovieListThunk(currentPage));
        dispatch(getBannerListThunk());
        dispatch(getMovieTheaterListThunk());
        dispatch(layThongTinLichChieuHeThongRapThunk());
    }, [dispatch, currentPage])

    if(isFetchingMovieList){
      return <div className="grid grid-cols-4 gap-20">
            {
                [... Array(16)].map((_,index) => (  
                    <Card key={index} className="!w-[280px]">
                        <Skeleton.Image active className="!w-full !h-[250px]"></Skeleton.Image>
                        <Skeleton.Input active className="!w-full !mt-10"></Skeleton.Input>
                        <Skeleton.Input active className="!w-full !mt-10"></Skeleton.Input>
                    </Card>
                ))
            }
        </div>
    }

    const onChange = (currentSlide: number) => {
      console.log(currentSlide);
    };

    const handlePageChange = (page: number) => {
      dispatch(quanLyPhimActions.setCurrentPage(page));
    };

  return (
    <div>
      <div className="mb-10 w-full mt-1">
        <Carousel afterChange={onChange} className=" mt-[81px]" autoplay>
          {
            bannerList.map((banner) => {
             const path = generatePath(PATH.detailmovie,{movieId: banner?.maPhim})  
            return <div key={banner?.maPhim} className="w-full lg:h-[650px]">
                <img src={banner?.hinhAnh} alt="..." className="cursor-pointer h-full" onClick={() => { navigate(path) }} />
              </div>
          })
          }
        </Carousel>
      </div>
      <div >
        <h2 className="font-serif uppercase text-center text-transparent  bg-clip-text text-3xl pb-10 bg-gradient-to-r to-zinc-800 from-zinc-600">
          Danh sách phim 
        </h2>
        <div className="w-fit mx-auto grid grid-cols-1 lg:grid-cols-4 md:grid-cols-2 justify-items-center justify-center gap-30">
          {
            moiveList?.map((movie) => {
              const path = generatePath(PATH.detailmovie,{movieId: movie?.maPhim})
              return <Card
                key={movie?.maPhim}
                hoverable
                style={{ width: 240 }}
                cover={<img style={{height: 320}} alt="..." src={movie?.hinhAnh}/>}
                onClick={() => { navigate(path) }}
              >
                <Card.Meta title={movie?.tenPhim} description={movie?.moTa.substring(0,50)} />
              </Card>
            })
          }
        </div>
      </div>
      <Pagination
        defaultCurrent={1}
        onChange={handlePageChange}
        total={50}
        className=" !my-4 text-center"
      />
      <div>
        <Detail></Detail>
      </div>
    </div>
  )
}

export default HomeTemplate

