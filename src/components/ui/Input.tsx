import { HTMLInputTypeAttribute } from "react"
import { UseFormRegister } from "react-hook-form"

type InputProps = {
    type?: HTMLInputTypeAttribute,
    placeholder?: string,
    register?: UseFormRegister<any>,
    name?: string,
    error?: string,
    label?: string,
    className?: string,
    disable?: boolean,
}

export const Input = ({type, placeholder, register, name, error, label, className, disable} : InputProps) => {
  return (
    <div className={className}>
      {
       label && <p className="mb-2">{label}</p> 
      }
      <input disabled={disable} type={type} className="bg-[#333] h-10 outline-none text-white leading-10 pl-4 text-sm rounded-lg text-16 block w-full p-2.5" placeholder={placeholder} 
      {... register(name)}
      />
      <p className='text-red-500'>{error}</p>
    </div>
  )
}

export default Input