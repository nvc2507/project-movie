import { apiInstance } from "constant";
import { Banner, Movie } from "types";

const api = apiInstance({
    baseURL: import.meta.env.VITE_QUAN_LY_PHIM,
})

export const quanLyPhimServices = {
    
    getMovieList: (query: string) =>
    api.get<ApiResponses<Movie[]>>(`/LayDanhSachPhimPhanTrang${query}`),
    getBannerList: () => api.get<ApiResponses<Banner[]>>('/LayDanhSachBanner'),
}